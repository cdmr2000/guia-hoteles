$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 3000
    })
    $('#contacto').on('show.bs.modal', function(e) {
        console.log("Se esta mostrando el modal contacto");
            $('#contactoBtn').removeClass('btn-outline-dark');
            $('#contactoBtn').addClass("btn-success");
            $('#contactoBtn').prop('disabled', true);
    })
    $('#contacto').on('shown.bs.modal', function(e){
        console.log("Se mostró el modal contacto");
    })
    $('#contacto').on('hide.bs.modal', function(e){
        console.log("Se oculta el modal contacto");
    })
    $('#contacto').on('hidden.bs.modal', function(e){
        console.log("Se ocultó el modal contacto");
            $('#contactoBtn').prop('disabled', false);
            $('#contactoBtn').removeClass('btn-success');
            $('#contactoBtn').addClass('btn-outline-dark');
    })
});